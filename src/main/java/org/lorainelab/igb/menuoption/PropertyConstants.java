/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.menuoption;

/**
 *
 * @author noorzahara
 */
public interface PropertyConstants {
    
    static final String PROP_FOREGROUND = "foreground";
    static final String PROP_BACKGROUND = "background";
    static final String PROP_POSITIVE_STRAND = "positive_strand_color";
    static final String PROP_NEGATIVE_STRAND = "negative_strand_color";
    static final String PROP_NAME_SIZE = "name_size";
    static final String PROP_SHOW_2TRACK = "show2tracks";
    static final String PROP_CONNECTED = "connected";
    static final String PROP_LABEL_FIELD = "label_field";
    static final String PROP_MAX_DEPTH = "max_depth";
    static final String PROP_DIRECTION_TYPE = "direction_type";
    static final String PROP_INDEX = "index";
    static final String PROP_DESCRIPTION = "description";
    static final String PROP_REFERENCE = "reference";
    static final String PROP_NAME = "name";
    static final String PROP_TITLE = "title";
    static final String PROP_URL = "url";
    static final String LOAD_HINT = "load_hint";

    static final String TAB = "\t";
    static final String EMPTY = "";
    static final String NEXT_LINE = "\n";

    public static enum DirectionType {

        NONE,
        ARROW,
        COLOR,
        BOTH;
    }
}
